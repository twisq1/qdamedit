#ifndef RDPPARSER_H
#define RDPPARSER_H

#include "processor.h"
#include "point.h"

class RDPparser : public Processor
{
public:
    RDPparser(Crosssection* crs);

    // Processor interface
    virtual QString getName() const override;

    bool getGlobalEnable() const override;
    void setGlobalEnable(bool enable) override;

private:
    static bool global_enable;

    virtual void doProcess() override;
    //void step(const QVector<Point*> &pointList, double epsilon, QVector<Point*> &out);
    void apply(const QList<Point*> &pointList, double epsilon);
};

#endif // RDPPARSER_H
