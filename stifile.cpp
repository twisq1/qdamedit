/**********************************************************************
* This file is part of qDAMEdit.
*
* qDAMEdit is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qDAMEdit is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qDAMEdit.  If not, see <http://www.gnu.org/licenses/>.
*
* qDAMEdit is written in Qt.
*
* Copyright 2020 TWISQ (https://www.twisq.nl)
***********************************************************************/

#include "stifile.h"

#include <QFile>
#include <QTextStream>
#include <QFileInfo>

#include <QDebug>

#include "point.h"

STIFile::STIFile()
{

}

bool STIFile::fromFile(const QString filename)
{
    QFile file(filename);
    if(!file.open(QIODevice::ReadOnly)) {
        m_readlog.append("Kan het bestand niet openen.");
        return false;
    }

    QFileInfo fi(filename);
    m_name = fi.fileName();

    QTextStream in(&file);

    ReadingMode rm = RM_NONE;
    while(!in.atEnd()) {
        QString line = in.readLine();

        if (line.startsWith("[END OF")) rm = RM_NONE;
        switch (rm) {
            case(RM_NONE):
                if(line.startsWith("[POINTS]")){
                    rm = RM_POINTS;
                    in.readLine(); //skip the line with the number points
                }
                else if(line.startsWith("[CURVES]")){
                    rm=RM_CURVES;
                    in.readLine(); //skip the line with the number curves
                }
                else if(line.startsWith("[BOUNDARIES]")){
                    rm=RM_BOUNDARIES;
                    in.readLine(); //skip the line with the number of boundaries
                }
                break;

            case(RM_POINTS):
                if (!handlePointLine(line)) {
                    rm = RM_NONE;
                }
                break;

            case(RM_CURVES):
                if (!handleCurveLine(line)) {
                    rm = RM_NONE;
                }
                break;

            case(RM_BOUNDARIES):
                if (line.length() < 2) {
                    rm = RM_NONE;
                } else {
                    //QString l1 = in.readLine(); //skip boundary number
                    try {
                        int numcurves = in.readLine().mid(0,8).trimmed().toInt();
                        int numlines = (numcurves-1) / 10 + 1;
                        QString bline;
                        for(int i=0;i<numlines;i++){
                            bline += in.readLine();
                        }
                        handleBoundaryLine(bline);
                    } catch (int) {
                        m_readlog.append(QString("Kan de regel %1 niet naar het aantal curves omzetten").arg(line));
                        return false;
                    }
                    //qDebug() << line;
                }
                break;
        }

    }

    return true;
}

Crosssection* STIFile::asCrosssection(Crosssection* crs)
{
    if(m_boundaries.length()==0) return crs;

    if (m_name.length() > 4) {
        crs->setName(m_name.left(m_name.length() - 4));
    } else {
        crs->setName(m_name);
    }
    STIBoundary sb = m_boundaries.at(m_boundaries.length()-1);
    QList<int> point_ids;
    for(int i=0; i<sb.curve_ids.count(); i++){
        STICurve c = m_curves[sb.curve_ids[i]-1]; //the curve id is always -1 from the index
        point_ids.append(c.p1);
        if(i==sb.curve_ids.count()-1)
            point_ids.append(c.p2);
    }

    //convert to crosssection points
    for(int i=0; i<point_ids.count(); i++){
        STIPoint p = m_points[point_ids[i]-1]; //the point id is always -1 from the index
        crs->addSurfacePoint(new Point(p.x, 0.0, p.y)); //note that the y value of the STI point is the z value of the crosssection point
    }

    return crs;
}

bool STIFile::handlePointLine(const QString line)
{
    if (line.length() <= 38) return false;
    bool ok1, ok2, ok3, ok4;
    int id = line.mid(0,8).trimmed().toInt(&ok1);
    double x = line.mid(8,15).trimmed().toDouble(&ok2);
    double y = line.mid(23,15).trimmed().toDouble(&ok3);
    double z = line.mid(38,15).trimmed().toDouble(&ok4);
    if (ok1 && ok2 && ok3 && ok4) {
        m_points.append(STIPoint{id,x,y,z});
        return true;
    } else {
        return false;
    }
}

bool STIFile::handleCurveLine(const QString line)
{
    if (line.length() <= 2) return false;
    bool ok1, ok2;
    int pid1 = line.mid(0,10).trimmed().toInt(&ok1);
    int pid2 = line.mid(11,6).trimmed().toInt(&ok2);
    if (ok1 && ok2) {
        m_curves.append(STICurve{m_curves.length()+1, pid1, pid2});
    }
    return true;
}

bool STIFile::handleBoundaryLine(const QString line)
{
    STIBoundary sb;
    try{
        sb.id = m_boundaries.length();
        QStringList args = line.split(" ", QString::SkipEmptyParts);
        foreach(QString arg, args){
            sb.curve_ids.append(arg.toInt());
        }
        m_boundaries.append(sb);
    }catch(int){
        m_readlog.append("Kan geen kaas maken van de boundary opmaak");
        return false;
    }
    return true;
}

