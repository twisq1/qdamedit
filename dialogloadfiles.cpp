#include "qsettings.h"
#include "qfiledialog.h"
#include "QMessageBox.h"
#include "qtextstream.h"
#include "project.h"
#include "dialogloadfiles.h"
#include "ui_dialogloadfiles.h"

DialogLoadFiles::DialogLoadFiles(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogLoadFiles)
{
    ui->setupUi(this);
}

DialogLoadFiles::~DialogLoadFiles()
{
    delete ui;
}

void DialogLoadFiles::on_buttonBox_rejected()
{
    close();
}

void DialogLoadFiles::on_buttonBox_accepted()
{
    accept();
}

QString DialogLoadFiles::sti_directoryname() const
{
    return ui->edit_dirname_sti->text();
}

QString DialogLoadFiles::database_filename() const
{
    return ui->edit_filename_database->text();
}

QString DialogLoadFiles::log_filename() const
{
    return ui->edit_filename_log->text();
}

QString DialogLoadFiles::characteristic_points_filename() const
{
    return ui->edit_filename_characteristicpoints->text();
}

QString DialogLoadFiles::surface_points_filename() const
{
    return ui->edit_filename_surfacepoints->text();
}

bool DialogLoadFiles::has_sti_directoryname() const
{
    return (ui->cb_sti->checkState() == Qt::CheckState::Checked);
}

bool DialogLoadFiles::has_database_filename() const
{
    return (ui->cb_database->checkState() == Qt::CheckState::Checked);
}

bool DialogLoadFiles::has_log_filename() const
{
    return (ui->cb_log->checkState() == Qt::CheckState::Checked);
}

bool DialogLoadFiles::has_characteristic_points_filename() const
{
    return (ui->cb_characteristicpoints->checkState() == Qt::CheckState::Checked);
}

bool DialogLoadFiles::has_surface_points_filename() const
{
    return (ui->cb_surfacepoints->checkState() == Qt::CheckState::Checked);
}

void DialogLoadFiles::on_bn_surfacepoints_clicked()
{
    QSettings settings;
    QString dir = settings.value("open_dir").toString();
    QString filename = QFileDialog::getOpenFileName(this, tr("Bestand openen..."), dir, tr("surfacepoint bestanden (*.csv)"));
    if(filename!=""){
        QString header = testFile(filename);
        if (!Project::isSurfacePointsHeader(header)) {
            QMessageBox::critical(nullptr, "Foutmelding", "Het bestand " + filename + " is geen geldig surfacepoints bestand.");
            return;
        }
        ui->cb_surfacepoints->setCheckable(true);
        ui->cb_surfacepoints->setCheckState(Qt::CheckState::Checked);
        ui->edit_filename_surfacepoints->setText(filename);
        QFileInfo fileinfo(filename);
        settings.setValue("open_dir", fileinfo.absolutePath());

    } else {
        ui->cb_surfacepoints->setCheckable(false);
        ui->cb_surfacepoints->setCheckState(Qt::CheckState::Unchecked);
        ui->edit_filename_surfacepoints->setText("");
    }
}

void DialogLoadFiles::on_bn_characteristic_clicked()
{
    QSettings settings;
    QString dir = settings.value("open_dir").toString();
    QString filename = QFileDialog::getOpenFileName(this, tr("Bestand openen..."), dir, tr("characteristic point bestanden (*.csv)"));
    if(filename!=""){
        QString header = testFile(filename);
        if (!Project::isCharacteristicPointsHeader(header)) {
            QMessageBox::critical(nullptr, "Foutmelding", "Het bestand " + filename + " is geen geldig characteristic points bestand.");
            return;
        }
        ui->cb_characteristicpoints->setCheckable(true);
        ui->cb_characteristicpoints->setCheckState(Qt::CheckState::Checked);
        ui->edit_filename_characteristicpoints->setText(filename);
        QFileInfo fileinfo(filename);
        settings.setValue("open_dir", fileinfo.absolutePath());

    } else {
        ui->cb_characteristicpoints->setCheckable(false);
        ui->cb_characteristicpoints->setCheckState(Qt::CheckState::Unchecked);
        ui->edit_filename_characteristicpoints->setText("");
    }
}

void DialogLoadFiles::on_bn_log_clicked()
{
    QSettings settings;
    QString dir = settings.value("open_dir").toString();
    QString filename = QFileDialog::getOpenFileName(this, tr("Bestand openen..."), dir, tr("log bestanden (*.csv)"));
    if(filename!=""){
        QString header = testFile(filename);
        if (!Project::isLogHeader(header)) {
            QMessageBox::critical(nullptr, "Foutmelding", "Het bestand " + filename + " is geen geldig log bestand.");
            return;
        }
        ui->cb_log->setCheckable(true);
        ui->cb_log->setCheckState(Qt::CheckState::Checked);
        ui->edit_filename_log->setText(filename);
        QFileInfo fileinfo(filename);
        settings.setValue("open_dir", fileinfo.absolutePath());

    } else {
        ui->cb_log->setCheckable(false);
        ui->cb_log->setCheckState(Qt::CheckState::Unchecked);
        ui->edit_filename_log->setText("");
    }
}

void DialogLoadFiles::on_bn_database_clicked()
{
    QSettings settings;
    QString dir = settings.value("open_dir").toString();
    QString filename = QFileDialog::getOpenFileName(this, tr("Bestand openen..."), dir, tr("Database bestanden (*.ded)"));
    if(filename!=""){
        ui->cb_database->setCheckable(true);
        ui->cb_database->setCheckState(Qt::CheckState::Checked);
        ui->edit_filename_database->setText(filename);
        QFileInfo fileinfo(filename);
        settings.setValue("open_dir", fileinfo.absolutePath());

    } else {
        ui->cb_database->setCheckable(false);
        ui->cb_database->setCheckState(Qt::CheckState::Unchecked);
        ui->edit_filename_database->setText("");
    }
}

void DialogLoadFiles::on_bn_sti_clicked()
{
    QSettings settings;
    QString dir = settings.value("sti_dir").toString();
    dir = QFileDialog::getExistingDirectory(this, tr("Selecteer pad met STI bestanden"), dir, QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    if(dir!=""){
        ui->cb_sti->setCheckable(true);
        ui->cb_sti->setCheckState(Qt::CheckState::Checked);
        ui->edit_dirname_sti->setText(dir);
        settings.setValue("sti_dir", dir);

    } else {
        ui->cb_sti->setCheckable(false);
        ui->cb_sti->setCheckState(Qt::CheckState::Unchecked);
        ui->edit_dirname_sti->setText("");
    }
}

QString DialogLoadFiles::testFile(QString &filename)
{
    QFile file(filename);

    if (!file.open(QIODevice::ReadOnly)) {
        return QString();
    }
    QTextStream in(&file);
    QString header = in.readLine();
    file.close();
    return header;
}
