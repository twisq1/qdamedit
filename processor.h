#ifndef PROCESSOR_H
#define PROCESSOR_H

#include <QString>

class Crosssection;

/****************************************************************************************************
 * This is the base class for all processors. You can subclass this class to make your own processor.
 * Every modification to the data should be done with a processor.
 * 2018-03-26 PT
 */

class Processor
{
public:
    Processor(Crosssection* crs);
    virtual ~Processor();

    // This virtual function returns the name of the processor. The name is to be displayed to the user.
    virtual QString getName() const = 0;

    // Call this function to start the processor. The actual work will be done by the doProcess function of the subclass.
    void run();

    // Getter and setter functions
    bool getEnabled() const;
    void setEnabled(bool enabled);
    bool getHasrun() const;
    bool getOk() const;
    QString getStatus() const;

    // These are the getters and setters of the global enable flag
    virtual bool getGlobalEnable() const = 0;
    virtual void setGlobalEnable(bool enable) = 0;

protected:
    // These private setters are to be used by a subclass only.
    void setOk(bool ok);
    void setStatus(const QString &status);

    // Error sets status message and sets ok to false;
    void error(const QString &status);
    Crosssection* crs() const { return m_crs; }

private:
    // This virtual function has to be implemented by the subclass. The actual work is done in this function.
    virtual void doProcess() = 0;

    // Member variables
    bool m_enabled;
    bool m_hasrun;
    bool m_ok;
    QString m_status;
    Crosssection* m_crs;
};

#endif // PROCESSOR_H
