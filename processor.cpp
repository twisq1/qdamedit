#include "crosssection.h"
#include "processor.h"

Processor::Processor(Crosssection* crs)
    : m_crs(crs)
{
    m_hasrun = false;
    m_enabled = false;
    m_ok = true;
    m_status = "Ok";
}

Processor::~Processor()
{
}

void Processor::run()
{
    if (getGlobalEnable() || getEnabled()) {
        doProcess();
        m_hasrun = true;
    }
}

bool Processor::getEnabled() const
{
    return m_enabled;
}

void Processor::setEnabled(bool enabled)
{
    if (enabled != m_enabled) {
        m_enabled = enabled;
    }
}

bool Processor::getHasrun() const
{
    return m_hasrun;
}

bool Processor::getOk() const
{
    return m_ok;
}

void Processor::setOk(bool ok)
{
    m_ok = ok;
}

QString Processor::getStatus() const
{
    return m_status;
}

void Processor::setStatus(const QString &status)
{
    m_status = status;
}

void Processor::error(const QString &status)
{
    setStatus(status);
    setOk(false);
}
