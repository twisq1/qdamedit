/**********************************************************************
* This file is part of qDAMEdit.
*
* qDAMEdit is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qDAMEdit is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qDAMEdit.  If not, see <http://www.gnu.org/licenses/>.
*
* qDAMEdit is written in Qt.
*
* Copyright 2020 TWISQ (https://www.twisq.nl)
***********************************************************************/

#include "comments.h"
#include <QFile>
#include <QTextStream>

// Definitions of standard comments.
QMap<int, QString> Comments::standard_comments;

Comments::Comments()
{
    freeText = "";
    commentsIds.clear();
}

// Load standard comment values.
void Comments::initialize_defaults()
{
    standard_comments.insert(1, "Bebouwing in profiel");
    standard_comments.insert(2, "Geen boezem in profiel");
    standard_comments.insert(3, "Geen kade herkenbaar in profiel");
    standard_comments.insert(4, "Geen teensloot");
    standard_comments.insert(5, "Hoog achterland");
    standard_comments.insert(6, "Hoogte<0,5m");
    standard_comments.insert(7, "Voorland is even hoog als kade");
    standard_comments.insert(8, "Vreemd profiel, moet bekeken worden");
}

bool Comments::initialize_from_inifile(const QString filepath)
{
    QFile f(filepath) ;
    if(!f.open(QIODevice::ReadOnly)){
        return false;
    }

    QTextStream in(&f);
    bool is_iniline = false;
    while(!in.atEnd()){
        QString line = in.readLine();

        if(!((line.trimmed().length()>0) && (line.trimmed()[0]=='#'))){
            if(line.contains("</STANDARD_COMMENTS>")){
                is_iniline = false;
                break;
            }
            if(is_iniline){
                QString comment = line.trimmed();
                if(comment.length()>0) standard_comments.insert(standard_comments.count()+1, comment);
            }
            if(line.contains("<STANDARD_COMMENTS>")) is_iniline = true;
        }
    }

    f.close();
    return standard_comments.count() > 0;
}

// Get a list with all standard comment id's.
QList<int> Comments::getAllStandardCommentIds()
{
    return standard_comments.keys();
}

// Look up a standard comment string by its id.
QString Comments::getStandardComment(const int id)
{
    return standard_comments.value(id);
}

// Load comments object from the given string. The string contains comments separated by slashes.
void Comments::fromString(const QString comments_string)
{
    QString s(comments_string);
    s.replace("//", "&slash;");
    freeText = "";
    commentsIds.clear();
    foreach (QString part, s.split("/")) {
        int id = standard_comments.key(part, 0);
        if (id > 0) {
            commentsIds.append(id);
        } else {
            if (!freeText.isEmpty()) freeText += "\n";
            part.replace("&slash;", "/");
            freeText.append(part);
        }
    }
}

// Return all selected standard comments and the freetext as one string. The values are separated by a slash.
QString Comments::toString() const
{
    QString result;
    foreach (int id, commentsIds) {
        if (!result.isEmpty()) result += "/";
        result+=standard_comments.value(id);
    }
    if (!freeText.isEmpty()) {
        QString v(freeText);
        v.replace("/", "//");
        v.replace("\n", "/");
        v.replace("\r", "/");

        if (!result.isEmpty()) result += "/";
        result += v;
    }
    return result;
}

// Get the list with selected standard comments.
QList<int> Comments::getCommentsIds() const
{
    return commentsIds;
}

// Set the list of selected standard comments.
// return true if the new list differs from the old list.
bool Comments::setCommentsIds(const QList<int> &value)
{
    // Test if the list is going to change.
    bool dirty = (commentsIds != value);
    commentsIds = value;
    return dirty;
}

// Get the free comment value.
QString Comments::getFreeText() const
{
    return freeText;
}

// Set the free comment value.
// return true if the new text differs from the old text.
bool Comments::setFreeText(const QString &value)
{
    // Test if text has to change.
    bool dirty = (freeText != value);
    freeText = value;
    return dirty;
}
