/**********************************************************************
* This file is part of qDAMEdit.
*
* qDAMEdit is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qDAMEdit is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qDAMEdit.  If not, see <http://www.gnu.org/licenses/>.
*
* qDAMEdit is written in Qt.
*
* Copyright 2020 TWISQ (https://www.twisq.nl)
***********************************************************************/

#ifndef CONSTANTS_H
#define CONSTANTS_H

#define ARABIC_RIGHT_TO_LEFT

#include <QString>
#include <QMap>

const int MAX_ZOOMFACTOR_X = 400;
const int MAX_ZOOMFACTOR_Y = 400;
const int MIN_ZOOMFACTOR_X = 10;
const int MIN_ZOOMFACTOR_Y = 10;
const int DEFAULT_ZOOMFACTOR_X = 100;
const int DEFAULT_ZOOMFACTOR_Y = 100;

const int CPOINT_SNAP_DISTANCE = 5; //afstand tussen muis en char point in x-richting waarbinnen de naam van het karakterisitieke punt weergegeven wordt

const double DEFAULT_RDP_EPSILON = 1.0;
const bool DEFAULT_SHOW_TRUNCATED_POINTS = true;

const int DEFAULT_EXPORT_IMAGE_WIDTH = 800;
const int DEFAULT_EXPORT_IMAGE_HEIGHT = 600;

const int SCALE_OPTIONS_1to1 = 0; //should follow zero based indexing
const int SCALE_OPTIONS_1toN = 1;
const int SCALE_OPTIONS_MAX = 2;

const int DEFAULT_SCALE_N = 10;


struct ImageSize{
    int width;
    int height;
    ImageSize(int w, int h){
        width = w;
        height = h;
    }
};

const ImageSize DEFAULT_IMAGESIZES[5] = {ImageSize(400,300), ImageSize(800,600), ImageSize(1280,720), ImageSize(1600,900), ImageSize(1920,1080)};
const static QMap<int, QString> DEFAULT_SCALE_OPTIONS = {{SCALE_OPTIONS_1to1, "1:1"}, {SCALE_OPTIONS_1toN, "1:n"}, {SCALE_OPTIONS_MAX, "maximaal"}};




#endif // CONSTANTS_H
