/**********************************************************************
* This file is part of qDAMEdit.
*
* qDAMEdit is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qDAMEdit is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qDAMEdit.  If not, see <http://www.gnu.org/licenses/>.
*
* qDAMEdit is written in Qt.
*
* Copyright 2020 TWISQ (https://www.twisq.nl)
***********************************************************************/

#include "point.h"
#include <math.h>

Point::Point()
{
    Point(0.0, 0.0, 0.0);
    m_code = 99;
    m_subcode = 999;
}

Point::Point(Point* p)
{
    m_x = p->x();
    m_y = p->y();
    m_z = p->z();
    m_code = 99;
    m_subcode = 999;
}

Point::Point(const double x, const double y, const double z)
{
    m_x = x;
    m_y = y;
    m_z = z;
    m_truncated = false;
    m_code = 99;
    m_subcode = 999;
}

double Point::calculateL(Point *base)
{
    double dx = m_x - base->x();
    double dy = m_y - base->y();
    return sqrt(dx*dx + dy*dy);
}

int Point::code() const
{
    return m_code;
}

void Point::setCode(int code)
{
    m_code = code;
}

int Point::subcode() const
{
    return m_subcode;
}

void Point::setSubcode(int subcode)
{
    m_subcode = subcode;
}
