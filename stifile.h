/**********************************************************************
* This file is part of qDAMEdit.
*
* qDAMEdit is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qDAMEdit is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qDAMEdit.  If not, see <http://www.gnu.org/licenses/>.
*
* qDAMEdit is written in Qt.
*
* Copyright 2020 TWISQ (https://www.twisq.nl)
***********************************************************************/

#ifndef STIFILE_H
#define STIFILE_H

#include <QString>
#include <QStringList>
#include "crosssection.h"

enum ReadingMode {RM_NONE, RM_POINTS, RM_CURVES, RM_BOUNDARIES};

struct STIPoint{
    int id;
    double x;
    double y;
    double z;
};

struct STICurve{
    int id;
    int p1;
    int p2;
};

struct STIBoundary{
    int id;
    QList<int> curve_ids;
};

class STIFile
{
public:
    STIFile();

    QStringList readlog() {return m_readlog;}
    bool fromFile(const QString filename);

    QString name() {return m_name;}

    Crosssection* asCrosssection(Crosssection* crs);


private:
    QStringList m_readlog;
    QString m_name;

    QList<STIPoint> m_points;
    QList<STICurve> m_curves;
    QList<STIBoundary> m_boundaries;

    bool handlePointLine(const QString line);
    bool handleCurveLine(const QString line);
    bool handleBoundaryLine(const QString line);

};

#endif // STIFILE_H
