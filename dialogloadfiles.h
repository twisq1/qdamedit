#ifndef DIALOGLOADFILES_H
#define DIALOGLOADFILES_H

#include <QDialog>

namespace Ui {
class DialogLoadFiles;
}

class DialogLoadFiles : public QDialog
{
    Q_OBJECT

public:
    explicit DialogLoadFiles(QWidget *parent = nullptr);
    ~DialogLoadFiles();

    bool has_surface_points_filename() const;
    bool has_characteristic_points_filename() const;
    bool has_log_filename() const;
    bool has_database_filename() const;
    bool has_sti_directoryname() const;
    QString surface_points_filename() const;
    QString characteristic_points_filename() const;
    QString log_filename() const;
    QString database_filename() const;
    QString sti_directoryname() const;

private slots:
    void on_buttonBox_rejected();
    void on_buttonBox_accepted();
    void on_bn_surfacepoints_clicked();

    void on_bn_characteristic_clicked();

    void on_bn_log_clicked();

    void on_bn_database_clicked();

    void on_bn_sti_clicked();

private:
    Ui::DialogLoadFiles *ui;
    static QString testFile(QString& filename);

};

#endif // DIALOGLOADFILES_H
