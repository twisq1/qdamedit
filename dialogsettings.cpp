/**********************************************************************
* This file is part of qDAMEdit.
*
* qDAMEdit is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qDAMEdit is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qDAMEdit.  If not, see <http://www.gnu.org/licenses/>.
*
* qDAMEdit is written in Qt.
*
* Copyright 2020 TWISQ (https://www.twisq.nl)
***********************************************************************/

#include "dialogsettings.h"
#include "ui_dialogsettings.h"

#include <QSettings>

#include "constants.h"


DialogSettings::DialogSettings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogSettings)
{
    ui->setupUi(this);    

    QSettings settings;
    m_epsilon = settings.value("rdpEpsilon", DEFAULT_RDP_EPSILON).toDouble();

    ui->dspEpsilon->setValue(m_epsilon);

    // set up default screen sizes
    for(auto imgsize : DEFAULT_IMAGESIZES){
        ui->cbDefaultImageSizes->addItem(QString("%1 x %2 px").arg(imgsize.width).arg(imgsize.height));
    }

    for(auto s : DEFAULT_SCALE_OPTIONS){
        ui->cbScale->addItem(s);
    }

    // adjust UI to settings
    ui->spImageWidth->setValue(settings.value("exportImageSizeWidth", DEFAULT_EXPORT_IMAGE_WIDTH).toInt());
    ui->spImageHeight->setValue(settings.value("exportImageSizeHeight", DEFAULT_EXPORT_IMAGE_HEIGHT).toInt());
    ui->cbScale->setCurrentIndex(settings.value("exportImageScaleOption",0).toInt());
    ui->dspScaleN->setValue(settings.value("exportImageScaleN",DEFAULT_SCALE_N).toInt());
}

DialogSettings::~DialogSettings()
{
    delete ui;
}

void DialogSettings::setGridSpacingX(const double spacing)
{
    m_grid_spacing_x = spacing;
    ui->dspinboxRasterX->setValue(m_grid_spacing_x);
}

void DialogSettings::setGridSpacingY(const double spacing)
{
    m_grid_spacing_y = spacing;
    ui->dspinboxRasterY->setValue(m_grid_spacing_y);
}

void DialogSettings::setWidthTrafficload(const double width)
{
    m_width_trafficload = width;
    ui->dspinboxWidthTrafficload->setValue(m_width_trafficload);
}

void DialogSettings::setHelperLineOffset(const double offset)
{
    m_helper_line_offset = offset;
    ui->dspinboxOffset->setValue(offset);
}

void DialogSettings::setShowTruncatedPoints(const bool show)
{
    m_show_truncated_points = show;
    ui->cbShowTruncated->setChecked(m_show_truncated_points);
}

void DialogSettings::accept()
{
    QSettings settings;
    settings.setValue("exportImageSizeWidth", ui->spImageWidth->value());
    settings.setValue("exportImageSizeHeight", ui->spImageHeight->value());
    settings.setValue("exportImageScaleOption", ui->cbScale->currentIndex());
    settings.setValue("exportImageScaleN", ui->dspScaleN->value());
    close();
}

void DialogSettings::on_dspinboxRasterX_valueChanged(double arg)
{
    m_grid_spacing_x = arg;
}

void DialogSettings::on_dspinboxRasterY_valueChanged(double arg)
{
    m_grid_spacing_y = arg;
}

void DialogSettings::on_dspinboxWidthTrafficload_valueChanged(double arg)
{
    m_width_trafficload = arg;
}

void DialogSettings::on_dspinboxOffset_valueChanged(double arg)
{
    m_helper_line_offset = arg;
}

void DialogSettings::on_cbShowTruncated_toggled(bool checked)
{
    m_show_truncated_points = checked;
}

void DialogSettings::on_dspEpsilon_valueChanged(double arg1)
{
    m_epsilon = arg1;
}

void DialogSettings::on_cbDefaultImageSizes_currentIndexChanged(int)
{
    setImageSizeSpinBoxes();
}

void DialogSettings::setImageSizeSpinBoxes()
{
    const ImageSize &imagesize = DEFAULT_IMAGESIZES[ui->cbDefaultImageSizes->currentIndex()];
    ui->spImageWidth->setValue(imagesize.width);
    ui->spImageHeight->setValue(imagesize.height);
}

void DialogSettings::on_cbScale_currentIndexChanged(int index)
{
    ui->dspScaleN->setEnabled(index==1);
}

void DialogSettings::on_cbDefaultImageSizes_highlighted(int index)
{
    Q_UNUSED(index);
    setImageSizeSpinBoxes();
}
