#include "rdpparser.h"

#include <QtMath>
#include <QSettings>

#include "point.h"
#include "crosssection.h"
#include "constants.h"


bool RDPparser::global_enable = true;

double PerpendicularDistance(Point *base, Point *pt, Point *lineStart, Point *lineEnd)
{
    double dx = lineEnd->calculateL(base) - lineStart->calculateL(base);
    double dy = lineEnd->z() - lineStart->z();

    //Normalise
    double mag = qPow(qPow(dx,2.0)+qPow(dy,2.0),0.5);
    if(mag > 0.0)
    {
        dx /= mag; dy /= mag;
    }

    double pvx = pt->calculateL(base) - lineStart->calculateL(base);
    double pvy = pt->z()  - lineStart->z();

    //Get dot product (project pv onto normalized direction)
    double pvdot = dx * pvx + dy * pvy;

    //Scale line direction vector
    double dsx = pvdot * dx;
    double dsy = pvdot * dy;

    //Subtract this from pv
    double ax = pvx - dsx;
    double ay = pvy - dsy;

    return qPow(qPow(ax,2.0)+qPow(ay,2.0),0.5);
}

RDPparser::RDPparser(Crosssection* crs) : Processor(crs)
{

}

QString RDPparser::getName() const
{
    return "RDPparser";
}

bool RDPparser::getGlobalEnable() const
{
    return global_enable;
}

void RDPparser::setGlobalEnable(bool enable)
{
    global_enable = enable;
}

void RDPparser::doProcess()
{
    if(crs()->numSurfacePoints() < 2){
        error("Het dwarsprofiel bevat minder dan 2 punten.");
        return;
    }

    QSettings settings;
    double epsilon = settings.value("rdpEpsilon", DEFAULT_RDP_EPSILON).toDouble();

    QList<CPoint*> cpoints = crs()->getCharacteristicPoints();
    QList<Point*> spoints = crs()->getSurfacePoints();

    //de processor reset eerst alle punten naar non truncated om zo
    //de mogelijkheid te bieden om met verschillende epsilons te spelen
    foreach(Point *p, spoints){
        p->setTruncated(false);
    }

    if(cpoints.count()<=0){ //easy, truncate from start until end
        apply(spoints, epsilon);
    }else{ //keep characteristic points intact
        QList<Point*> subline;
        subline.append(spoints.at(0));
        for(int i=1; i<spoints.count(); i++){
            subline.append(spoints.at(i));
            foreach(CPoint* cp, cpoints){
                if(cp->getL() == crs()->getSurfacePointKeys().at(i)){  // spoints[i]->getId()){ //getSurfacePointKeys
                    if(subline.count()>2)
                        apply(subline, epsilon);

                    subline.clear();
                    subline.append(spoints.at(i));
                }
            }
        }
    }
}


void RDPparser::apply(const QList<Point *> &pointList, double epsilon)
{
    // Find the point with the maximum distance from line between start and end
    double dmax = 0.0;
    int index = 0;
    int end = pointList.length()-1;
    for(int i = 1; i < pointList.length()-1; i++)
    {
        double d = PerpendicularDistance(pointList.first(), pointList[i], pointList[0], pointList[end]);
        if (d > dmax)
        {
            index = i;
            dmax = d;
        }
    }

    if(dmax > epsilon)
    {

        QList<Point*> firstLine;
        for(int i=0; i<(index+1); i++){if(!pointList.at(i)->truncated()) firstLine.append(pointList.at(i));}
        QList<Point*> lastLine;
        for(int i=index; i<pointList.length(); i++){if(!pointList.at(i)->truncated()) lastLine.append(pointList.at(i));}
        apply(firstLine, epsilon);
        apply(lastLine, epsilon);
    }else{ //alles tussen begin en eind valt binnen epsilon dus deze punten op truncated zetten
        for(int i=1; i<pointList.count()-1; i++){
            pointList.at(i)->setTruncated();
        }
    }
}

