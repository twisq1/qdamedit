/**********************************************************************
* This file is part of qDAMEdit.
*
* qDAMEdit is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qDAMEdit is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qDAMEdit.  If not, see <http://www.gnu.org/licenses/>.
*
* qDAMEdit is written in Qt.
*
* Copyright 2020 TWISQ (https://www.twisq.nl)
***********************************************************************/

#include "dialogstiimporter.h"
#include "ui_dialogstiimporter.h"

#include <QDebug>
#include <QDir>
#include <QFileInfo>

#include "stifile.h"

DialogSTIImporter::DialogSTIImporter(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogSTIImporter)
{
    ui->setupUi(this);
}

DialogSTIImporter::~DialogSTIImporter()
{
    delete ui;
}

void DialogSTIImporter::startImport()
{
    QDir dir(m_importdir);
    QFileInfoList fis = dir.entryInfoList(QStringList() << "*.sti" << "*.STI", QDir::Files);

    if (fis.length() == 0) {
        ui->pbarImportSTI->setMaximum(1);
        ui->pbarImportSTI->setValue(1);
        ui->tbImportSTI->append(QString("Geen STI bestanden gevonden in deze directory."));
    } else {

        ui->pbarImportSTI->setValue(0);
        ui->pbarImportSTI->setMaximum(fis.length());

        foreach(QFileInfo fi, fis) {
            if (!fi.exists()){
                ui->tbImportSTI->append(QString("Bestand %1 bestaat niet").arg(fi.absoluteFilePath()));
                break;
            }
            if (!fi.isFile()){
                ui->tbImportSTI->append(QString("Bestand %1 is geen bestand").arg(fi.absoluteFilePath()));
                break;
            }

            STIFile si = STIFile();
            if(!si.fromFile(fi.absoluteFilePath())){
                ui->tbImportSTI->append(QString("Fout bij het importeren van bestand %1;").arg(fi.fileName()));
                foreach(QString line, si.readlog()){
                    ui->tbImportSTI->append(line);
                }
            }else{
                m_project->addCrosssectionFromSTI(&si);
                ui->tbImportSTI->append(QString("Bestand %1 succesvol geimporteerd").arg(fi.fileName()));
            }

            ui->pbarImportSTI->setValue(ui->pbarImportSTI->value()+1);
        }
    }
}

void DialogSTIImporter::showEvent(QShowEvent *event)
{
     QDialog::showEvent(event);
     startImport();
}
