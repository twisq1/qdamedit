/**********************************************************************
* This file is part of qDAMEdit.
*
* qDAMEdit is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qDAMEdit is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qDAMEdit.  If not, see <http://www.gnu.org/licenses/>.
*
* qDAMEdit is written in Qt.
*
* Copyright 2020 TWISQ (https://www.twisq.nl)
***********************************************************************/

#include "crosssection.h"
#include "characteristicpoints.h"
#include "rdpparser.h"
#include <math.h>

Crosssection::Crosssection()
{
    m_deleted = false;
    m_comments = new Comments();
    m_processors.append(new RDPparser(this));
}

Crosssection::~Crosssection()
{
    // Delete all surface points.
    foreach(Point* p, m_points.values()) {
        delete p;
    }
    m_points.clear();

    // Delete all characteristic points.
    foreach (CPoint* cp, m_cpoints.values()) {
        delete cp;
    }
    m_cpoints.clear();

    // Delete all comments.
    delete m_comments;

    // Delete all processors.
    foreach (Processor* processor, m_processors) {
        delete processor;
    }
    m_processors.clear();
}

QList<Point *> Crosssection::getSurfacePoints(bool include_truncated)
{
    if(include_truncated){
        return m_points.values();
    }else{
        QList<Point*> result;
        foreach(Point* p, m_points.values()){
            if (!p->truncated()) result.append(p);
        }
        return result;
    }
}

Point *Crosssection::getSurfacePoint(uint32_t l)
{
    if (m_points.contains(l)) return m_points.value(l);
    else return nullptr;
}

bool Crosssection::getNearestSurfacePointPositions(uint32_t target, uint32_t *nearest, uint32_t *lower, uint32_t *higher)
{
    *lower = 0;
    *higher = (uint32_t) -1;
    if (!m_points.isEmpty()) {
        foreach(uint32_t i, m_points.keys()) {
            if (!m_points[i]->truncated()){
                if (i <= target) *lower = i;
                if (i >= target) {
                    *higher = i;
                    break;
                }
            }
        }
    }
    if ((target - *lower) < (*higher - target)) *nearest = *lower;
    else *nearest = *higher;
    return (target == *nearest);
}

QList<uint32_t> Crosssection::getSurfacePointKeys()
{
    return m_points.keys();
}

QList<CPoint *> Crosssection::getCharacteristicPoints()
{
    return m_cpoints.values();
}

int Crosssection::numSurfacePoints(bool include_truncated)
{
    if(include_truncated){
        return m_points.count();
    }else{
        int inum = 0;
        foreach (Point* p, m_points.values()) {
            inum += !p->truncated();
        }
        return inum;
    }
}

bool Crosssection::isDeleted() const
{
    return m_deleted;
}

void Crosssection::setDeleted(bool d)
{
    m_deleted = d;
}

uint32_t Crosssection::addSurfacePoint(Point *p)
{
    uint32_t l = 0;
    if (m_points.size()) {
        double distance = p->calculateL(m_points.first());
        l = (int)(distance * 1000);
    }
    if (m_points.contains(l)) {
        Point* old = m_points.value(l);
        if (old != p) delete old;
    }
    m_points.insert(l, p);
    return l;
}

uint32_t Crosssection::addSurfacePointBetween(const uint32_t middle, const uint32_t left, const uint32_t right)
{
    Q_ASSERT_X((middle >= left) && (right >= middle), "addSurfacePointBetween", "Requested position is not between left and right position,");

    // If we are on one of the edges, we don't need to add a point.
    if ((middle == left)  || (middle == right)) return middle;

    // Add point by using interpolation.
    Point* pleft = m_points.value(left);
    Point* pright = m_points.value(right);
    if (pleft && pright) {
        double factor = ((double) (middle - left)) / ((double) (right - left));
        double x = pleft->x() + factor * (pright->x() - pleft->x());
        double y = pleft->y() + factor * (pright->y() - pleft->y());
        double z = pleft->z() + factor * (pright->z() - pleft->z());
        Point* p = new Point(x, y, z);
        return addSurfacePoint(p);
    }
    return 0;
}

void Crosssection::addCharacteristicPoint(CPoint *point)
{
    // Check if such a point already exists. If so replace it.
    if (m_cpoints.contains(point->id())) delete m_cpoints.value(point->id());
    m_cpoints.insert(point->id(), point);
}

// Delete all characteristic points except maaiveld binnenwaarts and maaiveld buitenwaarts.
void Crosssection::deleteAllCharacteristicPoints()
{
    QList<CPoint*> cpoints = m_cpoints.values();
    m_cpoints.clear();
    foreach (CPoint* cp, cpoints) {
        if ((cp->id() != CPoint::MAAIVELD_BINNENWAARTS) && (cp->id() != CPoint::MAAIVELD_BUITENWAARTS)) {
            delete cp;
        } else {
            addCharacteristicPoint(cp);
        }
    }
}

double Crosssection::lmin()
{
    double result = 1e9;
    if (m_points.size()) {
        result = ((double)m_points.firstKey()) / 1000.0;
    }
    return result;

}

double Crosssection::lmax()
{
    double result = -1e9;
    if (m_points.size()) {
        result = ((double)m_points.lastKey() / 1000.0);
    }
    return result;
}

double Crosssection::zmin()
{
    double result = 1e9;
    foreach(Point* p, m_points.values()) {
        if (p->z() < result) result = p->z();
    }
    return result;
}

double Crosssection::zmax()
{
    double result = -1e9;
    foreach(Point* p, m_points.values()) {
        if (p->z() > result) result = p->z();
    }
    return result;
}

CPoint *Crosssection::getCharacteristicPointById(const int id)
{
    if (m_cpoints.contains(id)) return m_cpoints.value(id);
    else return nullptr;
}

// Calculate the height at a specific position of the cross section.
// Use interpolation.
double Crosssection::getZAt(const double l)
{
    if (m_points.size() < 2) return 0.0;
    Point *p1 = nullptr;
    Point *p2 = nullptr;
    double l1 = 0.0;
    double l2 = 0.0;
    foreach(uint32_t key, m_points.keys()) {
        p1 = p2;
        l1 = l2;
        p2 = m_points.value(key);
        l2 = ((double) key) / 1000.0;
        if (p1 && p2) {
            if((l1 <= l) && (l <= l2)){
                double z1 = p1->z();
                double z2 = p2->z();

                return z1 + (l - l1) / (l2 - l1) * (z2 - z1);
            }
        }
    }
    return 0.0;
}

void Crosssection::applyProcessors()
{
    foreach (Processor *processor, m_processors) {
        processor->run();
        if(!processor->getOk()){

        }
    }
}

bool Crosssection::hasTruncatedPoints()
{
    foreach (Point* p, m_points) {
        if(p->truncated())
            return true;
    }
    return false;
}

