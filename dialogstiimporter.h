/**********************************************************************
* This file is part of qDAMEdit.
*
* qDAMEdit is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qDAMEdit is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qDAMEdit.  If not, see <http://www.gnu.org/licenses/>.
*
* qDAMEdit is written in Qt.
*
* Copyright 2020 TWISQ (https://www.twisq.nl)
***********************************************************************/

#ifndef DIALOGSTIIMPORTER_H
#define DIALOGSTIIMPORTER_H

#include <QDialog>
#include "project.h"

namespace Ui {
class DialogSTIImporter;
}

class DialogSTIImporter : public QDialog
{
    Q_OBJECT

public:
    explicit DialogSTIImporter(QWidget *parent = nullptr);
    ~DialogSTIImporter();

    void setImportDir(const QString s) {m_importdir = s;}
    void setProject(Project *prj) {m_project = prj;}

private:
    Ui::DialogSTIImporter *ui;
    Project *m_project;
    QString m_importdir;

    void startImport();
    void showEvent(QShowEvent* event);

};

#endif // DIALOGSTIIMPORTER_H
