/**********************************************************************
* This file is part of qDAMEdit.
*
* qDAMEdit is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* qDAMEdit is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with qDAMEdit.  If not, see <http://www.gnu.org/licenses/>.
*
* qDAMEdit is written in Qt.
*
* Copyright 2020 TWISQ (https://www.twisq.nl)
***********************************************************************/

#ifndef DIALOGSETTINGS_H
#define DIALOGSETTINGS_H

#include <QDialog>

namespace Ui {
class DialogSettings;
}

class DialogSettings : public QDialog
{
    Q_OBJECT

public:
    explicit DialogSettings(QWidget *parent = nullptr);
    ~DialogSettings();

    void setGridSpacingX(const double spacing);
    void setGridSpacingY(const double spacing);
    void setWidthTrafficload(const double width);
    void setHelperLineOffset(const double offset);
    void setShowTruncatedPoints(const bool show);

    double getGridSpacingX() {return m_grid_spacing_x;}
    double getGridSpacingY() {return m_grid_spacing_y;}
    double getWidthTrafficload() {return m_width_trafficload;}
    double getHelperLineOffset() {return m_helper_line_offset;}
    bool getShowTruncatedPoints() {return m_show_truncated_points;}
    double getEpsilon() {return m_epsilon;}

protected:
    void accept();

private slots:
    void on_dspinboxRasterX_valueChanged(double arg);
    void on_dspinboxRasterY_valueChanged(double arg);
    void on_dspinboxWidthTrafficload_valueChanged(double arg);
    void on_dspinboxOffset_valueChanged(double arg);

    void on_cbShowTruncated_toggled(bool checked);

    void on_dspEpsilon_valueChanged(double arg1);

    void on_cbDefaultImageSizes_currentIndexChanged(int);

    void on_cbScale_currentIndexChanged(int index);


    void on_cbDefaultImageSizes_highlighted(int index);

private:
    Ui::DialogSettings *ui;

    double m_grid_spacing_x;
    double m_grid_spacing_y;
    double m_width_trafficload;
    double m_helper_line_offset;
    bool m_show_truncated_points;
    double m_epsilon;

    void setImageSizeSpinBoxes();
};

#endif // DIALOGSETTINGS_H
